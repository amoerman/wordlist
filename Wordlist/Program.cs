﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Wordlist {
  public class Program {
    public static void Main(string[] args) {
      IList<IWord> words = ReadWords().ToList();
      var stopwatch = new Stopwatch();
      stopwatch.Start();
      const int requestedLength = 6;
      // index all words by their length
      ILookup<int, IWord> wordsByLength = words.ToLookup(w => w.Length);
      // create a set of all words of the desired total length. This makes "Contains" checks blazing fast
      var wordEqualityComparer = new WordEqualityComparer();
      ISet<IWord> wordsOfRequestedLength = new HashSet<IWord>(wordsByLength[requestedLength], wordEqualityComparer);

      var combinations = Enumerable.Range(1, requestedLength - 1)
        // start with all words of length "i"
        .SelectMany(i => wordsByLength[i]
          // create a combination with all other words of length "total - i"
          .SelectMany(word => wordsByLength[requestedLength - i].Select(otherWord => new WordCombination(word, otherWord)))
          // verify that combination is in the list
          .Where(combination => wordsOfRequestedLength.Contains(combination) )
        ) 
        // materialize the results
        .ToList();

      stopwatch.Stop();

      Console.WriteLine("Combinations: " + combinations.Count);
      Console.WriteLine("Time taken: " + stopwatch.ElapsedMilliseconds + "ms");
      Console.WriteLine("Press [enter] to see all combinations");
      Console.ReadLine();
      Console.WriteLine(string.Join(Environment.NewLine, combinations));
      Console.ReadLine();
    }


    private static IEnumerable<IWord> ReadWords() {
      var assembly = Assembly.GetExecutingAssembly();
      var resourceName = "Wordlist.wordlist.txt";

      using (var stream = assembly.GetManifestResourceStream(resourceName)) {
        using (var reader = new StreamReader(stream, Encoding.UTF8)) {
          string line;
          while ((line = reader.ReadLine()) != null) {
            yield return new Word(line);
          }
        }
      }
    }
  }
}