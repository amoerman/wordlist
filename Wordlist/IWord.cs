﻿namespace Wordlist {
  public interface IWord {
    string Content { get; }
    int Length { get; }
  }
}